/*
 Navicat Premium Data Transfer

 Source Server         : smana
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : smana

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 16/05/2022 17:36:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES (1, 'admin', '管理员', '123456', 'admin@qq.com', '12345673', '江西赣州', '2022-02-01 00:58:34', '/static/wang.png', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for active
-- ----------------------------
DROP TABLE IF EXISTS `active`;
CREATE TABLE `active`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of active
-- ----------------------------
INSERT INTO `active` VALUES (1, 10, 's5454', 'string', 'string', 'string', '2022-03-26 06:42:46');

-- ----------------------------
-- Table structure for approve
-- ----------------------------
DROP TABLE IF EXISTS `approve`;
CREATE TABLE `approve`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apply_student_id` int(11) NULL DEFAULT NULL,
  `apply_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `teacher_approve_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'approving、approved、confuted、cancel',
  `teacher_approve2_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'approving、approved、confuted、cancel',
  `approve_log_id` int(11) NULL DEFAULT NULL COMMENT '批准的日志id',
  `approve_log2_id` int(11) NULL DEFAULT NULL COMMENT '批准的日志2id',
  `doc_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文档路径',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of approve
-- ----------------------------
INSERT INTO `approve` VALUES (1, 1, '入党积极分子', '申请成为入党积极分子', '表现优秀', '2022-03-31 23:24:30', 'Approved', 'Approved', 20, 20, NULL, 'Approved');
INSERT INTO `approve` VALUES (4, 1, '共青团员', '23', '1421', '0001-01-01 00:00:00', 'Confuted', 'Approving', 16, NULL, NULL, 'Confuted');
INSERT INTO `approve` VALUES (5, 1, '入党积极分子', '124', '124', '2022-04-01 13:35:48', 'Approved', 'Approving', 18, NULL, NULL, 'Cancel');
INSERT INTO `approve` VALUES (25, 11, '中共党员', '大僧达', '该agadsgasdfasd', '2022-05-15 10:33:10', 'Confuted', 'Approving', 15, NULL, 'static/论文.doc', 'Confuted');
INSERT INTO `approve` VALUES (26, 11, '共青团员', '申请成为共青团员', '表现优越, 热爱组织, 热爱国家, 想为国家尽一份力.', '2022-05-16 16:05:09', 'Approved', 'Approved', 22, 21, 'static/修改意见.doc', 'Approved');

-- ----------------------------
-- Table structure for approve_log
-- ----------------------------
DROP TABLE IF EXISTS `approve_log`;
CREATE TABLE `approve_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '事件类型',
  `other` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `student_id` int(11) NULL DEFAULT NULL,
  `teacher_id` int(11) NULL DEFAULT NULL,
  `approve_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of approve_log
-- ----------------------------
INSERT INTO `approve_log` VALUES (2, '符合条件', '职位申请', '3:1', '2022-04-05 20:57:10', NULL, 1, NULL);
INSERT INTO `approve_log` VALUES (3, '符合条件', '职位申请', '4:1', '2022-04-05 21:01:25', NULL, 1, NULL);
INSERT INTO `approve_log` VALUES (4, '符合条件', '职位申请', '8:11', '2022-04-05 21:07:44', NULL, 16, NULL);
INSERT INTO `approve_log` VALUES (5, '不符合条件', '职位申请', '0:1', '2022-04-05 20:56:19', NULL, 16, NULL);
INSERT INTO `approve_log` VALUES (6, '', '职位申请', '1:1', '2022-05-13 20:49:41', 1, 1, 4);
INSERT INTO `approve_log` VALUES (7, '', '职位申请', '1:1', '2022-05-13 20:56:56', 1, 1, 3);
INSERT INTO `approve_log` VALUES (8, '', '职位申请', '1:1', '2022-05-13 20:58:55', 1, 1, 1);
INSERT INTO `approve_log` VALUES (9, '', '职位申请', '1:1', '2022-05-13 21:02:21', 1, 1, 5);
INSERT INTO `approve_log` VALUES (10, '', '职位申请', '1:1', '2022-05-13 21:03:59', 1, 1, 5);
INSERT INTO `approve_log` VALUES (14, '', '职位申请', '1:1', '2022-05-15 11:51:11', 1, 1, 1);
INSERT INTO `approve_log` VALUES (15, '', '职位申请', '1:11', '2022-05-15 11:51:25', 11, 1, 25);
INSERT INTO `approve_log` VALUES (16, '', '职位申请', '1:1', '2022-05-15 11:51:44', 1, 1, 4);
INSERT INTO `approve_log` VALUES (17, '教师id 1 批准 申请id 1批准理由:符合条件', '职位申请', '1:1', '2022-05-15 21:11:55', 1, 1, 1);
INSERT INTO `approve_log` VALUES (18, '教师id 1 批准 申请id 5批准理由:符合条件', '职位申请', '1:1', '2022-05-15 21:28:10', 1, 1, 5);
INSERT INTO `approve_log` VALUES (19, '学生[1]取消申请[5]', '取消申请', NULL, '2022-05-15 23:29:47', 1, NULL, 5);
INSERT INTO `approve_log` VALUES (20, '教师id 16 批准 申请id 1批准理由:符合条件', '职位申请', '16:1', '2022-05-16 12:51:35', 1, 16, 1);
INSERT INTO `approve_log` VALUES (21, '教师id 17 批准 申请id 26批准理由:符合条件', '职位申请', '17:11', '2022-05-16 16:06:13', 11, 17, 26);
INSERT INTO `approve_log` VALUES (22, '教师id 1 批准 申请id 26批准理由:符合条件', '职位申请', '1:11', '2022-05-16 16:34:27', 11, 1, 26);

-- ----------------------------
-- Table structure for dict
-- ----------------------------
DROP TABLE IF EXISTS `dict`;
CREATE TABLE `dict`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dict
-- ----------------------------
INSERT INTO `dict` VALUES ('user', 'el-icon-user', 'icon');
INSERT INTO `dict` VALUES ('house', 'el-icon-house', 'icon');
INSERT INTO `dict` VALUES ('menu', 'el-icon-menu', 'icon');
INSERT INTO `dict` VALUES ('s-custom', 'el-icon-s-custom', 'icon');
INSERT INTO `dict` VALUES ('s-grid', 'el-icon-s-grid', 'icon');
INSERT INTO `dict` VALUES ('document', 'el-icon-document', 'icon');

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `size` bigint(20) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_delete` tinyint(255) NULL DEFAULT 0,
  `enable` tinyint(255) NULL DEFAULT 1,
  `md5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES (8, 'QQ截图20220122184708.jpg', 'jpg', 128, 'http://localhost:9090/file/c5f65c7c6e7145f1a2a409c3903483f7.jpg', 1, 0, '8996b9618b87f5f3630d08974d8a612e', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (9, '用户信息.xlsx', 'xlsx', 9, 'http://localhost:9090/file/25b5c032dd574c519afde1c7c8b7e9c3.xlsx', 0, 1, '15c09b91d04d3d3c34fd53f0949ac35d', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (10, 'config.yml', 'yml', 644, 'http://localhost:9090/file/422ddfb1ed91458b8e10d5f9959bdfab.yml', 0, 1, '062364991f5f925b02d53636902288ae', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (11, '相框权限.jpg', 'jpg', 37, 'http://localhost:9090/file/d4924674353245dd97c4543084664226.jpg', 0, 1, '4d5675019e2bb4927b13b3110fa17439', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (12, '权限.txt', 'txt', 9, 'http://localhost:9090/file/e71910fa8c4a42cba1dec2ba86cd69ee.txt', 0, 1, 'fb50ff2bbaa69e59ffa968e4d2f5dd25', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (13, '90180612162 黄成洵.PNG', 'PNG', 971, 'http://localhost:9090/file/40e3d5aa31f64446a4d7f925c077a4af.PNG', 0, 1, 'dae78ad3a2043748c1cbfef87b601182', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (14, '34d674177d7b8dc903a0f3116205ac95.jpeg', 'jpeg', 9, 'http://localhost:9090/file/e169016bc11a4ab48a556cf8e150dce9.jpeg', 0, 1, '05f7f795c4a1169f31b805fc00ec4975', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (15, '1.jpeg', 'jpeg', 255, 'http://localhost:9090/file/42d567ec967846df89bb7a16003a666d.jpeg', 0, 1, 'e4a8985c38c372304c981a3a46249ebf', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (16, '34d674177d7b8dc903a0f3116205ac95.jpeg', 'jpeg', 9, 'http://localhost:9090/file/e169016bc11a4ab48a556cf8e150dce9.jpeg', 0, 1, '05f7f795c4a1169f31b805fc00ec4975', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (17, '1.jpeg', 'jpeg', 255, 'http://localhost:9090/file/42d567ec967846df89bb7a16003a666d.jpeg', 0, 1, 'e4a8985c38c372304c981a3a46249ebf', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (18, '34d674177d7b8dc903a0f3116205ac95.jpeg', 'jpeg', 9, 'http://localhost:9090/file/e169016bc11a4ab48a556cf8e150dce9.jpeg', 0, 1, '05f7f795c4a1169f31b805fc00ec4975', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (19, '34d674177d7b8dc903a0f3116205ac95.jpeg', 'jpeg', 9, 'http://localhost:9090/file/e169016bc11a4ab48a556cf8e150dce9.jpeg', 0, 1, '05f7f795c4a1169f31b805fc00ec4975', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (20, '1.jpeg', 'jpeg', 255, 'http://localhost:9090/file/42d567ec967846df89bb7a16003a666d.jpeg', 0, 1, 'e4a8985c38c372304c981a3a46249ebf', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (21, '1.jpeg', 'jpeg', 255, 'http://localhost:9090/file/42d567ec967846df89bb7a16003a666d.jpeg', 0, 1, 'e4a8985c38c372304c981a3a46249ebf', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (22, '1.jpeg', 'jpeg', 255, 'http://localhost:9090/file/42d567ec967846df89bb7a16003a666d.jpeg', 0, 1, 'e4a8985c38c372304c981a3a46249ebf', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (23, '图四.jpg', 'jpg', 114, 'http://localhost:9090/file/0b8a4d5ed9e3486b9cb9473252369dc2.jpg', 0, 1, '079f731b124e7892b408ee452a1295ca', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (24, 'QQ图片20220218171649.jpg', 'jpg', 55, 'http://localhost:9090/file/9aa32cb7ba504aea97bd17cc8e22f7f2.jpg', 0, 1, '4160a9dda4bec90bfd549f6c89a0d768', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (25, 'QQ图片20220218195329.jpg', 'jpg', 2, 'http://localhost:9090/file/9c3c574efe0945f9856abbbfd67612c0.jpg', 0, 1, '71f8f96a2e1914afeafdef2af6bb63e7', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (26, 'QQ图片20220218201611.jpg', 'jpg', 1, 'http://localhost:9090/file/471dee15bbdf42f48416c68ccecf2e7c.jpg', 0, 1, 'f971e4aeece4bbf18d6e117b294f68b9', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (27, 'QQ图片20220219020021.jpg', 'jpg', 1, 'http://localhost:9090/file/2373ec38e5c0491295b7aeb572ce1eba.jpg', 0, 1, '8c1e620c51b39b4275641b2df0040631', '2022-03-11 16:13:13');
INSERT INTO `file` VALUES (28, '可视化网络爬虫发展的开发_KOVIAZIN MIKHAIL.caj', '.caj', 5654964, 'UploadFile/可视化网络爬虫发展的开发_KOVIAZIN MIKHAIL.caj', 0, 1, 'b4e6d3cb89307f732fb3ffa80b677eaa', '0001-01-01 00:00:00');
INSERT INTO `file` VALUES (29, '多线程垂直搜索引擎网络爬虫软件V1.0_雷霖.caj', '.caj', 147826, 'UploadFile/多线程垂直搜索引擎网络爬虫软件V1.0_雷霖.caj', 0, 1, '9cdf289f755ec8923b28aa6728b94378', '2022-03-11 17:38:30');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid` int(11) NULL DEFAULT NULL COMMENT '父级id',
  `page_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '成绩分析', '/home', 'el-icon-user', '111', NULL, 'Home');
INSERT INTO `menu` VALUES (3, '系统管理', NULL, 'el-icon-s-grid', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (8, '角色管理', '/role', 'el-icon-user', '', 3, 'Role');
INSERT INTO `menu` VALUES (9, '用户管理', '/user', 'el-icon-s-custom', NULL, 3, 'User');
INSERT INTO `menu` VALUES (10, '菜单管理', '/menu', 'el-icon-menu', NULL, 3, 'Menu');
INSERT INTO `menu` VALUES (11, '文件管理', '/file', 'el-icon-document', NULL, 3, 'File');
INSERT INTO `menu` VALUES (14, '成绩查询', '/grade', 'el-icon-document', '成绩查询', NULL, 'grade');
INSERT INTO `menu` VALUES (15, '成绩录入', '/gradein', 'el-icon-document', '成绩录入', NULL, 'gradein');
INSERT INTO `menu` VALUES (16, '学生管理', '/studnet', 'el-icon-user', '学生管理', 3, 'Studnet');
INSERT INTO `menu` VALUES (17, '教师管理', '/teacher', 'el-icon-user', NULL, 3, 'Teacher');

-- ----------------------------
-- Table structure for notify
-- ----------------------------
DROP TABLE IF EXISTS `notify`;
CREATE TABLE `notify`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `publish_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `publish_level` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '发布等级',
  `publish_id` int(11) NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notify
-- ----------------------------
INSERT INTO `notify` VALUES (1, 'test', '测试数据', 'ACCOUNT', '紧急,提示,信息', 1, '2022-03-22 11:59:48');
INSERT INTO `notify` VALUES (7, '测试一下', '<blockquote><p>发动赶快和;催下警方莱森蓝海</p></blockquote>', 'ACCOUNT', '提示', 1, '2022-05-16 10:18:01');

-- ----------------------------
-- Table structure for process
-- ----------------------------
DROP TABLE IF EXISTS `process`;
CREATE TABLE `process`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `approve_id` int(11) NULL DEFAULT NULL COMMENT '申请id',
  `student_id` int(11) NULL DEFAULT NULL COMMENT '申请人id',
  `teacher_id` int(11) NULL DEFAULT NULL COMMENT '批准人id',
  `teacher2_id` int(11) NULL DEFAULT NULL COMMENT '批准人2id',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '申请的职位',
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of process
-- ----------------------------
INSERT INTO `process` VALUES (3, 25, 11, 1, 17, '中共党员', '2022-05-15 23:18:00');
INSERT INTO `process` VALUES (4, 4, 1, 1, 16, '共青团员', '2022-05-15 23:18:00');
INSERT INTO `process` VALUES (5, 1, 1, 1, 16, '入党积极分子', '2022-05-16 12:51:36');
INSERT INTO `process` VALUES (6, 26, 11, 1, 17, '共青团员', '2022-05-16 16:34:27');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `flag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '管理员', '管理员', 'ROLE_ADMIN');
INSERT INTO `role` VALUES (2, '教师', '教师', 'ROLE_TEA');
INSERT INTO `role` VALUES (3, '学生', '学生', 'ROLE_STU');

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES (1, 1);
INSERT INTO `role_menu` VALUES (1, 3);
INSERT INTO `role_menu` VALUES (1, 8);
INSERT INTO `role_menu` VALUES (1, 9);
INSERT INTO `role_menu` VALUES (1, 10);
INSERT INTO `role_menu` VALUES (1, 14);
INSERT INTO `role_menu` VALUES (1, 15);
INSERT INTO `role_menu` VALUES (1, 16);
INSERT INTO `role_menu` VALUES (1, 17);
INSERT INTO `role_menu` VALUES (2, 1);
INSERT INTO `role_menu` VALUES (2, 3);
INSERT INTO `role_menu` VALUES (2, 14);
INSERT INTO `role_menu` VALUES (2, 15);
INSERT INTO `role_menu` VALUES (2, 16);
INSERT INTO `role_menu` VALUES (3, 1);
INSERT INTO `role_menu` VALUES (3, 14);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cls` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `identity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `native` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `politics` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `t1_id` int(11) NULL DEFAULT NULL COMMENT '培养人1',
  `t2_id` int(11) NULL DEFAULT NULL COMMENT '培养人1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `oneG`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '202202001', '李四', '123456', '二班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '123', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 1, 16);
INSERT INTO `student` VALUES (3, '202203001', '王五', '123456', '三班', 'ROLE_STU', '1fsd212@qq.com', '1535345432', '1', '/static/wang.png', '2022-02-09 13:11:58', '362143200112044012', '男', '江西', '群众', 16, 1);
INSERT INTO `student` VALUES (4, '202201003', '谢悦林', '123456', '一班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '11', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 1, 16);
INSERT INTO `student` VALUES (11, '202202004', '王封', '123456', '二班', 'ROLE_STU', '123@qq.com', '1535345432', '北京海淀区', '/static/wang.png', '2022-02-01 01:12:01', '362143200112044012', '男', '江西', '群众', 1, 17);
INSERT INTO `student` VALUES (48, '202202005', '李四发', '123456', '二班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '123', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 17, 1);
INSERT INTO `student` VALUES (49, '202203002', '王含', '123456', '三班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '1', '/static/wang.png', '2022-02-09 13:11:58', '362143200112044012', '男', '江西', '群众', NULL, NULL);
INSERT INTO `student` VALUES (50, '202201007', '谢悦林', '123456', '一班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '11', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 16, 1);
INSERT INTO `student` VALUES (51, '202202008', '王冉封', '123456', '二班', 'ROLE_STU', '123@qq.com', '1535345432', '北京海淀区', '/static/wang.png', '2022-02-01 01:12:01', '362143200112044012', '男', '江西', '群众', 1, 17);
INSERT INTO `student` VALUES (52, '202202009', '李四积', '123456', '二班', 'ROLE_STU', '123fsd212@qq.com', '1535345432', '123', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 16, 1);
INSERT INTO `student` VALUES (53, '202203010', '王看', '123456', '三班', 'ROLE_STU', '1fsd212@qq.com', '1535345432', '1', '/static/wang.png', '2022-02-09 13:11:58', '362143200112044012', '男', '江西', '群众', NULL, NULL);
INSERT INTO `student` VALUES (54, '202201011', '谢悦发', '123456', '一班', 'ROLE_STU', 'fsd212@qq.com', '1535345432', '11', '/static/wang.png', '2022-02-22 13:12:01', '362143200112044012', '男', '江西', '群众', 1, 17);
INSERT INTO `student` VALUES (55, '202202012', '王冉汗', '123456', '二班', 'ROLE_STU', '123@qq.com', '1535345432', '北京海淀区', '/static/wang.png', '2022-02-01 01:12:01', '362143200112044012', '男', '江西', '群众', 17, 1);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cls` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cdate` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `one`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, '20181001', '胡兰梦', '123456', '一班', NULL, 'fre2012@qq.com', '18456564545', '风举动感动了解放啦；开机。', '/static/wang.png', '2022-02-17 02:31:01');
INSERT INTO `teacher` VALUES (16, '20181002', '农梦安', '123456', '二班', NULL, 'fd3@qq.com', '15354646474', '发动嘎咚发动', '/static/wang.png', '2022-03-25 04:29:22');
INSERT INTO `teacher` VALUES (17, '20181003', '符动', '123456', '三班', NULL, 're2111@qq.com', '15423567878', '发动afdafads', NULL, '2022-05-15 10:39:26');

SET FOREIGN_KEY_CHECKS = 1;
