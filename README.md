# SManagement

#### 介绍

使用 C# webapi，vue3，前后端分离。

#### 截图

##### 登录

![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/login.jpg "图片")

##### 学生

![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/student/1%20(1).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/student/1%20(2).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/student/1%20(3).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/student/1%20(4).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/student/1%20(5).jpg "图片")

##### 教师

![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/teacher/1%20(1).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/teacher/1%20(2).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/teacher/1%20(3).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/teacher/1%20(4).jpg "图片")

##### 管理员

![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(1).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(2).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(3).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(4).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(5).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(6).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(7).jpg "图片")
![alt 图片](https://gitee.com/FlameLing/smanagement/raw/master/imgs/admin/1%20(8).jpg "图片")
