﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using SManaModel.utils;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.utils.Tests
{
    [TestClass()]
    public class ReflectionExtensionTests
    {

        class Person
        {
            public Person(string name, string pwd, int age, DateTime cdate)
            {
                this.name = name;
                this.pwd = pwd;
                this.age = age;
                this.cdate = cdate;
            }

            public string name { get; set; }
            public string pwd { get; set; }
            public int age { get; set; }
            public DateTime cdate { get; set; }
        }

        class Person2
        {
            public string Name { get; set; }

            [System.ComponentModel.Description("pwd")]
            public string password { get; set; }

            public int Age { get; set; }
            [System.ComponentModel.Description("cdate")]
            public DateTime CreateDate { get; set; }
        }

        [TestMethod()]
        public void ConvertToTest()
        {
            Person p1 = new("张三", "123456", 15, DateTime.Now);

            var p2 = p1.ConvertTo<Person2>();

            Assert.IsNotNull(p2);
        }
    }
}