import axios from "../../utils/axios";

export default {
  ["getTeachers"](data) {
    data.type = "teacher";
    return axios.get("user", {
      params: data,
    });
  },
  ["getTeacher"](data) {
    data.type = "teacher";
    return axios.get("user/obj", {
      params: data,
    });
  },
  ["addTeacher"](data) {
    data.type = "teacher";
    return axios.post("user", data);
  },
  ["delTeachers"](data) {
    data.type = "teacher";
    return axios.delete("user", {
      data: data,
    });
  },
  ["updateTeacher"](data) {
    data.type = "teacher";
    return axios.put("user", data);
  },
  ["getSelTeachers"]() {
    return axios.get("user/tea_select");
  },
};