import axios from "../../utils/axios";

export default {
  ["login"](data) {
    return axios.post("/login", data);
  },
  ["logout"]() {
    return axios.get("/logout");
  },
  ["save"](data) {
    data.type = data.type.toLowerCase();
    return axios.put("/user", data);
  },
  ["changePwd"](data) {
    data.type = data.type.toLowerCase();
    return axios.put("/user/cpwd", data);
  },
};
