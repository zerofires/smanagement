import axios from "../../utils/axios";

// 奖惩服务
export default  {
    ["getRewards"](data) {
      return axios.get("notify", {
        params: data,
      });
    },
    ["delReward"](data) {
      let paras = "";
      for (let key of Object.keys(data)) {
        paras += `${key}=${data[key]}&`;
      }
      return axios.delete(`notify?${paras}`);
    },
    ["updateReward"](data) {
      return axios.put("notify", data);
    },
    ["addReward"](data) {
      return axios.post("notify", data);
    },
  };