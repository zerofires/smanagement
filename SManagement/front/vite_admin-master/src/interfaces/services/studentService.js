import axios from "../../utils/axios";

export default  {
    ["getStudents"](data) {
      data.type = "student";
      return axios.get("user", {
        params: data,
      });
    },
    ["getStudent"](data) {
      data.type = "student";
      return axios.get("user/obj", {
        params: data,
      });
    },
    ["addStudent"](data) {
      data.type = "student";
      return axios.post("user", data);
    },
    ["delStudents"](data) {
      data.type = "student";
      return axios.delete("user", {
        data: data,
      });
    },
    ["updateStudent"](data) {
      data.type = "student";
      return axios.put("user", data);
    },
  };