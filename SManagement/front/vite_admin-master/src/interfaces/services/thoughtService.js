import axios from "../../utils/axios";

// 思想服务
export default  {
    ["getThought"](data) {
      return axios.get("active", {
        params: data,
      });
    },
    ["delThought"](data) {
      let paras = "";
      for (let key of Object.keys(data)) {
        paras += `${key}=${data[key]}&`;
      }
      // FIXME: 后台还没确定格式
      return axios.delete(`active?${paras}`);
    },
    ["updateThought"](data) {
      return axios.put("active", data);
    },
    ["addThought"](data) {
      return axios.post("active", data);
    },
  };