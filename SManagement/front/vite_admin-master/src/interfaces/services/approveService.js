import axios from "../../utils/axios";

export default {
  ["getApprovesForTeacher"](data) {
    return axios.get("approve/teacher", {
      params: data,
    });
  },
  ["getApprovesForStudent"](data) {
    return axios.get("approve/student", {
      params: data,
    });
  },
  ["delApprove"](data) {
    let paras = "";
    for (let key of Object.keys(data)) {
      paras += `${key}=${data[key]}&`;
    }
    return axios.delete(`approve?${paras}`);
  },
  ["applyApprove"](id, order, ok) {
    const data = {
      id,
      order,
      ok
    }
    return axios.get(`approve/apply`, {
      params: data,
    });
  },
  ["updateApprove"](data) {
    return axios.put("approve", data);
  },
  ["addApprove"](data) {
    return axios.post("approve", data);
  },
  ['process']() {
    return axios.get("approve/process");
  },
  ['assignment'](data) {
    return axios.post("approve/assign", data);
  },
  ['cancel'](id) {
    return axios.get("approve/cancel", {
      params: {
        id
      }
    });
  },
};