import axios from "../../utils/axios";

export default {
    ["getNotifies"](data) {
      return axios.get("notify", {
        params: data,
      });
    },
    ["delNotify"](data) {
      let paras = "";
      for (let key of Object.keys(data)) {
        paras += `${key}=${data[key]}&`;
      }
      return axios.delete(`notify?${paras}`);
    },
    ["updateNotify"](data) {
      return axios.put("notify", data);
    },
    ["addNotify"](data) {
      return axios.post("notify", data);
    },
  };