import approveService from "./services/approveService";
import loginService from "./services/loginService";
import notifyService from "./services/notifyService";
import rewardService from "./services/rewardService";
import studentService from "./services/studentService";
import teacherService from "./services/teacherService";
import thoughtService from "./services/thoughtService";

export {
  approveService,
  loginService,
  notifyService,
  rewardService,
  studentService,
  teacherService,
  thoughtService,
};
