import RouteView from "../../components/RouteView.vue";

export default [{
  path: "student/process",
  name: "StudentProcess",
  meta: {
    title: "发展进程",
    icon: "Box",
    roles: ["STUDENT"],
  },
  component: () => import("../../views/student/Process.vue"),
}, {

  path: "student/notify",
  name: "StudentNotify",
  meta: {
    title: "通知信息",
    icon: "UserFilled",
    roles: ["STUDENT"],
  },
  component: () => import("../../views/student/TableNotify.vue"),
}, {
  path: "student",
  name: "StudentIndex",
  meta: {
    title: "学生信息",
    icon: "User",
    roles: ["STUDENT"],
  },
  component: RouteView,
  children: [{

      path: "approve",
      name: "StudentApprove",
      meta: {
        title: "申请信息",
        roles: ["STUDENT"],
      },
      component: () => import("../../views/student/TableApprove.vue"),
    },
    {
      path: "apply",
      name: "StudentApply",
      // hidden: true,
      meta: {
        title: "填写申请",
        roles: ["STUDENT"],
      },
      component: () => import("../../views/student/AddApprove.vue"),
    },
  ],
}]