import RouteView from "../../components/RouteView.vue";

export default [{
  path: "account/notify",
  name: "NotifyForAccountModule",
  meta: {
    title: "通知管理",
    icon: "Bell",
    roles: ["ACCOUNT"],
  },
  component: RouteView,
  children: [{
      path: "manage",
      name: "ManageNotifyForAccount",
      meta: {
        title: "通知信息",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/TableNotify.vue"),
    },
    {
      path: "add",
      name: "AddNotifyForAccount",
      meta: {
        title: "发布通知",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/AddNotify.vue"),
    },
  ],
}, {
  path: "account/teacher",
  name: "TeacherForAccountModule",
  meta: {
    title: "教师管理",
    icon: "UserFilled",
    roles: ["ACCOUNT"],
  },
  component: RouteView,
  children: [{
      path: "manage",
      name: "ManageTeacherForAccount",
      meta: {
        title: "教师信息",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/TableTeacher.vue"),
    },
    {
      path: "update/:id",
      name: "UpdateTeacherForAccount",
      hidden: true,
      meta: {
        title: "修改信息",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/UpdateTeacher.vue"),
    },
    {
      path: "add",
      name: "AddTeacherForAccount",
      meta: {
        title: "添加教师",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/AddTeacher.vue"),
    },
  ],
}, {
  path: "account/student",
  name: "StudentForAccountModule",
  meta: {
    title: "学生管理",
    icon: "User",
    roles: ["ACCOUNT"],
  },
  component: RouteView,
  children: [{
      path: "manage",
      name: "ManageStudentForAccount",
      meta: {
        title: "学生信息",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/TableStudent.vue"),
    },
    {
      path: "update/:id",
      name: "UpdateStudentForAccount",
      hidden: true,
      meta: {
        title: "更新学生",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/UpdateStudent.vue"),
    },
    {
      path: "add",
      name: "AddStudentForAccount",
      meta: {
        title: "添加学生",
        roles: ["ACCOUNT"],
      },
      component: () => import("../../views/account/AddStudent.vue"),
    },
  ],
}, ]