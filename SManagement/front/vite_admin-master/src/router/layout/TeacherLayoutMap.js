import RouteView from "../../components/RouteView.vue";

export default [{
  path: "teacher/approve",
  name: "ManageApproveForTeacher",
  meta: {
    title: "审批管理",
    icon: "Box",
    roles: ["TEACHER"],
  },
  component: () => import("../../views/teacher/TableApprove.vue"),
}, {

  path: "teacher/notify",
  name: "NotifyForTeacher",
  meta: {
    title: "通知信息",
    icon: "Bell",
    roles: ["TEACHER"],
  },
  component: () => import("../../views/teacher/TableNotify.vue"),
}, {
  path: "teacher/student",
  name: "StudentForTeacherModule",
  meta: {
    title: "学生管理",
    icon: "User",
    roles: ["TEACHER"],
  },
  component: RouteView,
  children: [{
      path: "manage",
      name: "ManageStudentForTeacher",
      meta: {
        title: "学生信息",
        roles: ["TEACHER"],
      },
      component: () => import("../../views/teacher/TableStudent.vue"),
    },
    {
      path: "student/update/:id",
      name: "UpdateStudentForTeacher",
      hidden: true,
      meta: {
        title: "更新学生",
        roles: ["TEACHER"],
      },
      component: () => import("../../views/teacher/UpdateStudent.vue"),
    },
    {
      path: "student/add",
      name: "AddStudentForTeacher",
      meta: {
        title: "添加学生",
        roles: ["TEACHER"],
      },
      component: () => import("../../views/teacher/AddStudent.vue"),
    },
  ],
}, ]