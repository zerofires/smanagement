import Layout from "../layout/Index.vue";
import studentLayoutMap from "./layout/StudentLayoutMap"
import teacherLayoutMap from "./layout/TeacherLayoutMap"
import accountLayoutMap from "./layout/AccountLayoutMap"


const layoutMap = [
  ...studentLayoutMap,
  ...teacherLayoutMap,
  ...accountLayoutMap,
  {
    path: "user",
    name: "User",
    hidden: true /* 不在侧边导航展示 */ ,
    meta: {
      title: "个人中心",
    },
    component: () => import("../views/common/User.vue"),
  },
  {
    path: "changePwd",
    name: "ChangePassword",
    hidden: true /* 不在侧边导航展示 */ ,
    meta: {
      title: "修改密码",
    },
    component: () => import("../views/common/ChangePwd.vue"),
  },
  {
    path: "/error",
    name: "NotFound",
    hidden: true,
    meta: {
      title: "404",
    },
    component: () => import("../components/NotFound.vue"),
  },
  {
    path: "/:w+",
    hidden: true,
    redirect: {
      name: "NotFound",
    },
  },
  {
    path: "player",
    name: "Player",
    meta: {
      title: "视频播放",
      icon: "Film",
    },
    component: () => import("../views/common/XGPlayer.vue"),
  },
  // {
  //   path: "charts",
  //   name: "Charts",
  //   meta: {
  //     title: "数据图表",
  //     icon: "trend-charts",
  //   },
  //   component: () => import("../views/data/Charts.vue"),
  // },
];

const routes = [{
    path: "/login",
    name: "Login",
    meta: {
      title: "登录",
    },
    component: () => import("../views/login/Login.vue"),
  },
  {
    path: "/",
    name: "Layout",
    component: Layout,
    children: [...layoutMap],
  },
];

export {
  routes,
  layoutMap
};



// {
//   path: "active",
//   name: "Active",
//   component: RouteView,
//   hidden: true,
//   meta: {
//     title: "活动管理",
//     icon: "DataLine",
//   },
//   children: [{
//       path: "reward",
//       name: "Reward",
//       hidden: true,
//       meta: {
//         title: "奖惩管理",
//         roles: ["ACCOUNT", "TEACHER", "STUDENT"],
//       },
//       component: () => import("../views/active/Reward.vue"),
//     },
//     {
//       path: "thought",
//       name: "Thought",
//       meta: {
//         title: "思想报告",
//         roles: ["ACCOUNT", "TEACHER", "STUDENT"],
//       },
//       component: () => import("../views/active/Thought.vue"),
//     },
//   ],
// },