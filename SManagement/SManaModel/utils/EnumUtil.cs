﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.utils
{
    public static class EnumUtil
    {
        public static TEnum? ToEnum<TEnum>(this string str)
        {
            return Enum.TryParse(typeof(TEnum), str, true, out var result) ? (TEnum)result : default;
        }
    }
}
