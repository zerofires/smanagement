﻿using SManaModel.dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.models
{
    public class PageComponent
    {
        /// <summary>
        /// 总数量
        /// </summary>
        public int Total { get; set; } = -1;
        /// <summary>
        /// 当前页
        /// </summary>
        public int PageNum { get; set; }
        /// <summary>
        /// 页面大小
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 跳过多少
        /// </summary>
        public int SkipSize { get; set; }


        public static PageComponent Create(int pageNum, int pageSize = 10, int total = -1)
        {
            var page = new PageComponent();
            page.PageNum = pageNum;
            page.Total = total;
            page.PageSize = pageSize;
            page.SkipSize = page.PageNum < 1 ? 0 : (page.PageNum - 1) * page.PageSize;
            return page;
        }

        public static PageComponent Create(PageDto pageDto)
        {
            return Create(pageDto.PageNum, pageDto.PageSize);
        }

        public static IDictionary<string, string>? GetCondition(string? condition)
        {
            if (string.IsNullOrEmpty(condition))
                return null;

            var conditions = new Dictionary<string, string>();
            return conditions;
        }
    }
}
