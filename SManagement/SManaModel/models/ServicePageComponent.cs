﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.models
{
    /// <summary>
    /// 分页服务组件
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class ServicePageComponent<TEntity, TKey> where TEntity : class
    {
        /// <summary>
        /// 分页组件
        /// </summary>
        public PageComponent? PageComponent { get; set; }

        /// <summary>
        /// 条件表达式
        /// </summary>
        public Expression<Func<TEntity, bool>>? WhereExpression { get; set; }

        /// <summary>
        /// 排序表达式
        /// </summary>
        public Expression<Func<TEntity, TKey>>? OrderByExpression { get; set; }

        /// <summary>
        /// 是否正序排序
        /// </summary>
        public bool IsAscending { get; set; } = true;

        /// <summary>
        /// 创建组件
        /// </summary>
        /// <param name="pageComponent">分页组件</param>
        /// <param name="orderByExpression">排序表达式</param>
        /// <param name="whereExpression">条件表达式</param>
        /// <param name="isAscending">是否正序排序, 默认是</param>
        /// <returns></returns>
        public static ServicePageComponent<TEntity, TKey> Create(
            PageComponent? pageComponent,
            Expression<Func<TEntity, TKey>>? orderByExpression,
            Expression<Func<TEntity, bool>>? whereExpression = null,
            bool isAscending = false)
        {

            return new ServicePageComponent<TEntity, TKey>
            {
                OrderByExpression = orderByExpression,
                PageComponent = pageComponent,
                WhereExpression = whereExpression,
                IsAscending = isAscending
            };
        }
    }
}
