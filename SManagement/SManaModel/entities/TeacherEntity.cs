﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    [Table("teacher")]
    public class TeacherEntity
    {
        [Key]
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? Name { get; set; }

        public string? Cls { get; set; }

        public string? Role { get; set; }

        public string? Email { get; set; }

        public string? Phone { get; set; }

        public string? Address { get; set; }

        public string? Password { get; set; }

        [Column(name: "avatar_url")]
        public string? AvatarUrl { get; set; }

        public DateTime CDate { get; set; } = DateTime.Now;

        //[NotMapped]
        //public List<StudentEntity>? StudentEntities { get; set; }
    }
}
