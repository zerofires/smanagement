﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace SManaModel.entities.logs
{
    public class EFLogger : ILogger,IDisposable
    {
        private readonly string _categoryName;

        public EFLogger(string categoryName) => this._categoryName = categoryName;

        public bool IsEnabled(LogLevel logLevel) => true;
        public IDisposable BeginScope<TState>(TState state) => this;

        public void Log<TState>(LogLevel logLevel,
            EventId eventId,
            TState state,
            Exception? exception,
            Func<TState, Exception, string> formatter)
        {
            //var logContent = formatter(state, exception);
            //Console.WriteLine();
            //Console.WriteLine(logContent);

            if (_categoryName == DbLoggerCategory.Database.Command.Name &&
                logLevel == LogLevel.Information)
            {
                var logContent = formatter(state, exception!);

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(logContent);
                Console.ResetColor();
            }

        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
