﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    [Table("approve")]
    public class ApproveEntity
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 学生id
        /// </summary>
        [Column("apply_student_id")]
        public int ApplyStudentId { get; set; }

        [ForeignKey(nameof(ApplyStudentId))]
        public virtual StudentEntity? ApplyStudent { get; set; }

        /// <summary>
        /// 申请职位
        /// </summary>
        [Column("apply_position")]
        public string? ApplyPosition { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string? Title { get; set; }

        /// <summary>
        /// 申请理由
        /// </summary>
        public string? Reason { get; set; }

        /// <summary>
        /// 申请状态 approving、approved、confuted、cancel
        /// </summary>
        [Column("teacher_approve_status")]
        public string? TeacherApproveStatus { get; set; } = TeacherApproveStatusEnum.Approving.ToString();

        [Column("teacher_approve2_status")]
        public string? TeacherApprove2Status { get; set; } = TeacherApproveStatusEnum.Approving.ToString();

        /// <summary>
        /// 文档路径
        /// </summary>
        [Column("doc_path")]
        public string? DocPath { get; set; }

        /// <summary>
        /// 是否批准
        /// </summary>
        [Column("status")]
        public string? Status { get; set; } = ApproveStatusEnum.Approving.ToString();

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CDate { get; set; } = DateTime.Now;

        [Column("approve_log_id")]
        public int? ApproveLogId { get; set; }
        [NotMapped]
        [ForeignKey(nameof(ApproveLogId))]
        public virtual List<ApproveLogEntity>? ApproveLog_1 { get; set; }

        [Column("approve_log2_id")]
        public int? ApproveLog2Id { get; set; }
        [NotMapped]
        [ForeignKey(nameof(ApproveLog2Id))]
        public virtual List<ApproveLogEntity>? ApproveLog_2 { get; set; }
    }

    public enum ApproveStatusEnum 
    {
        Approving,
        Approved,
        Confuted,
        Cancel,
    }

    public enum TeacherApproveStatusEnum
    {
        Approving,
        Approved,
        Confuted,
    }
}
