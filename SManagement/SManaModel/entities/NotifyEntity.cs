﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    [Table("notify")]
    public class NotifyEntity
    {
        [Key]
        public int Id { get; set; }

        public string? Title { get; set; }

        [Column(name: "publish_type")]
        public string? PublishType { get; set; }

        [Column(name: "publish_level")]
        public string? PublishLevel { get; set; }

        public string? Content { get; set; }

        public DateTime CDate { get; set; } = DateTime.Now;

        [Column(name: "publish_id")]
        public int? PublishId { get; set; }
        [ForeignKey(nameof(PublishId))]
        public virtual AccountEntity? Account { get; set; }
    }
}
