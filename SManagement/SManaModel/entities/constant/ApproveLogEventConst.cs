﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities.constant
{
    public class ApproveLogEventConst
    {
        public const string Apply = "职位申请";
        public const string CancelApply = "取消申请";
        public const string Approve = "批准申请";
    }
}
