﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    /// <summary>
    /// 批准日志
    /// </summary>
    [Table("approve_log")]
    public class ApproveLogEntity
    {
        public ApproveLogEntity()
        {
            
        }

        public ApproveLogEntity(string? content, string? @event, string? other, int? studentId, StudentEntity student, int? teacherId, int? approveId)
        {
            Content = content;
            Event = @event;
            Other = other;
            StudentId = studentId;
            Student = student;
            TeacherId = teacherId;
            ApproveId = approveId;
        }

        public ApproveLogEntity(string? content, string? @event, int? studentId, int? approveId)
        {
            Content = content;
            Event = @event;
            StudentId = studentId;
            ApproveId = approveId;
        }


        [Key]
        public int Id { get; set; } = 0;

        /// <summary>
        /// 日志内容
        /// </summary>
        public String? Content { get; set; }

        /// <summary>
        /// 日志事件名
        /// </summary>
        public String? Event { get; set; }

        /// <summary>
        /// 其他信息
        /// </summary>
        public String? Other { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CDate { get; set; } = DateTime.Now;



        [Column("student_id")]
        public int? StudentId { get; set; }
        [NotMapped]
        [ForeignKey(nameof(StudentId))]
        public StudentEntity Student { get; set; }

        [Column("teacher_id")]
        public int? TeacherId { get; set; }
        [NotMapped]
        [ForeignKey(nameof(StudentId))]
        public TeacherEntity Teacher { get; set; }

        [Column("approve_id")]
        public int? ApproveId { get; set; }
        [NotMapped]
        [ForeignKey(nameof(ApproveId))]
        public ApproveEntity Approve { get; set; }
    }


   
}
