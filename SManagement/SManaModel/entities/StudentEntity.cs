﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    [Table("student")]
    public class StudentEntity
    {
        public StudentEntity()
        {
            CDate = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? Name { get; set; }

        public string? Cls { get; set; }

        public string? Role { get; set; }

        public string? Email { get; set; }

        public string? Phone { get; set; }

        public string? Address { get; set; }

        public string? Password { get; set; }

        /// <summary>
        /// 身份
        /// </summary>
        public string? Identity { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string? Gender { get; set; }

        /// <summary>
        /// 籍贯
        /// </summary>
        public string? Native { get; set; }

        /// <summary>
        /// 政治面貌
        /// </summary>
        public string? Politics { get; set; }

        [Column(name: "avatar_url")]
        public string? AvatarUrl { get; set; }

        public DateTime CDate { get; set; } = DateTime.Now;


        [Column("t1_id")]
        public int? Teacher_Id { get; set; }
        [ForeignKey(nameof(Teacher_Id))]
        public TeacherEntity? Teacher { get; set; }

        [Column("t2_id")]
        public int? Teacher2_Id { get; set; }
        [ForeignKey(nameof(Teacher2_Id))]
        public TeacherEntity? Teacher2 { get; set; }

        [InverseProperty(nameof(ApproveEntity.ApplyStudent))]
        public virtual ICollection<ApproveEntity>? Approves { get; set; }
    }
}
