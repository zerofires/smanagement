﻿using Microsoft.EntityFrameworkCore;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SManaModel.entities;
using SManaModel.entities.logs;

namespace SManaModel.entities
{
    public class MyDbContext : DbContext
    {
        public DbSet<StudentEntity>? Student { get; set; }
        public DbSet<TeacherEntity>? Teacher { get; set; }
        public DbSet<AccountEntity>? Account { get; set; }
        public DbSet<FileEntity>? File { get; set; }
        public DbSet<NotifyEntity>? Notify { get; set; }
        public DbSet<ProcessEntity>? Process { get; set; }
        public DbSet<ActiveEntity>? Active { get; set; }
        public DbSet<ApproveEntity>? Approve { get; set; }
        public DbSet<ApproveLogEntity>? ApproveLog { get; set; }


        public MyDbContext(DbContextOptions<MyDbContext> options)
        : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new EFLoggerProvider());
            optionsBuilder.EnableSensitiveDataLogging(true);
            optionsBuilder.UseLoggerFactory(loggerFactory);

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<StudentEntity>(entity =>
            //{
            //    entity.HasOne(t => t.Teacher1).WithMany(t=>t.StudentEntities).HasForeignKey(t => t.Teacher_1_Id);
            //    entity.HasOne(t => t.Teacher2).WithMany(t=>t.StudentEntities).HasForeignKey(t => t.Teacher_2_Id);
            //});

            //builder.Entity<TeacherEntity>(entity =>
            //{
            //    entity.HasMany(t => t.StudentEntities).WithOne(t => t.Teacher1).HasForeignKey(t => t.Teacher_1_Id);
            //    entity.HasMany(t => t.StudentEntities).WithOne(t => t.Teacher2).HasForeignKey(t => t.Teacher_2_Id);
            //});
            base.OnModelCreating(builder);
        }

    }
}
