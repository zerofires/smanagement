﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities;

[Table("active")]
public class ActiveEntity
{
    [Key]
    public int Id { get; set; }

    [Column("teacher_id")]
    public int TeacherId { get; set; }

    public string? Title { get; set; }

    public string? Content { get; set; }

    public string? Description { get; set; }

    /// <summary>
    /// 文档路径
    /// </summary>
    [Column("file_path")]
    public string? FilePath { get; set; }

    public DateTime CDate { get; set; }
}
