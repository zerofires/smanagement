﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    [Table("file")]
    public class FileEntity
    {
        [Key]
        public int Id { get; set; }

        public string? Name { get; set; }
        [Description]
        public string? Type { get; set; }

        public long Size { get; set; }

        public string? Url { get; set; }

        public ushort Is_Delete { get; set; } = 0;

        public ushort Enable { get; set; } = 1;

        public string? Md5 { get; set; }

        public DateTime CDate { get; set; } = DateTime.Now;
    }
}
