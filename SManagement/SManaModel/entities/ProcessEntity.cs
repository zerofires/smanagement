﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.entities
{
    /// <summary>
    /// 发展历程
    /// </summary>
    [Table("process")]
    public class ProcessEntity
    {
        public ProcessEntity()
        {
            CDate = DateTime.Now;
        }

        /// <summary>
        /// 进程id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 申请id
        /// </summary>
        [Column("approve_id")]
        public int ApproveId { get; set; }

        /// <summary>
        /// 申请人
        /// </summary>
        [Column("student_id")]
        public int StudentId { get; set; }
        [ForeignKey(nameof(StudentId))]
        public virtual StudentEntity? Student { get; set; }

        /// <summary>
        /// 审批人id
        /// </summary>
        [Column("teacher_id")]
        public int TeacherId { get; set; }
        [ForeignKey(nameof(TeacherId))]
        public virtual TeacherEntity? Teacher { get; set; }

        /// <summary>
        /// 审批人2id
        /// </summary>
        [Column("teacher2_id")]
        public int Teacher2Id { get; set; }
        [ForeignKey(nameof(Teacher2Id))]
        public virtual TeacherEntity? Teacher2 { get; set; }

        /// <summary>
        /// 申请位置
        /// </summary>
        [Column("position")]
        public string Position { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("cdate")]
        public DateTime CDate { get; set; }
    }
}
