﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel
{
    public class ApiResult
    {
        public ApiResult()
        {
        }

        public ApiResult(bool success, int code, string? message, object? data)
        {
            this.success = success;
            this.code = code;
            this.message = message;
            this.data = data;
        }

        public bool success { get; set; }
        public int code { get; set; }
        public string? message { get; set; }
        public object? data { get; set; }


        public static ApiResult Ok(string? msg = null, object? data = null)
        {
            return new ApiResult(true, (int)ApiResultCode.OK, msg, data);
        }

        public static ApiResult Page(object list, int totalNum, string? msg = null)
        {
            return ApiResult.Page(new PageApiResult
            {
                list = list,
                totalNum = totalNum,
            }, msg);
        }

        public static ApiResult Page(PageApiResult? page, string? msg = null)
        {
            return ApiResult.Ok(msg, page);
        }

        public static ApiResult Fail(string msg, object? data = null, int code = (int)ApiResultCode.FAIL)
        {
            return new ApiResult(true, code, msg, data);
        }

        public static ApiResult FailOfParams(object? data = null, int code = (int)ApiResultCode.FAIL)
        {
            return Fail("参数错误");
        }

        public static ApiResult Judge(int n, string? succMsg = null, string failMsg = "操作失败, 请联系管理员!")
        {
            return n > 0 ? ApiResult.Ok(succMsg, data: n) : ApiResult.Fail(failMsg, data: n);
        }

        public static ApiResult Error(string msg, object? data = null, int code = (int)ApiResultCode.ERROR)
        {
            return new ApiResult(false, code, msg, data);
        }
    }

    public enum ApiResultCode
    {
        OK = 0,
        // fail 1~99
        FAIL = 1,
        // error 100~199
        ERROR = 100,
    }

    public class PageApiResult
    {
        public int totalNum { get; set; }
        public object list { get; set; }
    }
}
