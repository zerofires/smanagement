﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.dtos
{

    public class ApproveDto
    {
        public int id { get; set; }

        [Description("ApplyPosition")]
        public string? position { get; set; }

        public string? title { get; set; }

        public string? reason { get; set; }

        public DateTime cdate { get; set; }

        public string? doc_path { get; set; }

        /// <summary>
        /// 申请状态 Approving、Approved、Confuted、Cancel
        /// </summary>
        public string? tstatus { get; set; }

        public string? t2status { get; set; }

        public string? status { get; set; }

        public int? sid { get; set; }

        public string? sname { get; set; }

        public int? t1id { get; set; }

        public string? t1name { get; set; }

        public int? t2id { get; set; }

        public string? t2name { get; set; }
    }
}
