﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.dtos
{
    public class PageDto 
    {
        [DefaultValue("1")]
        public int PageNum { get; set; } = 1;

        [DefaultValue("10")]
        public int PageSize { get; set; } = 10;
    }
}
