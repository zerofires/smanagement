﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.dtos
{
    public class SearchPageDto : PageDto
    {
        public IDictionary<string, string>? Conditions { get; set; }
    }
}
