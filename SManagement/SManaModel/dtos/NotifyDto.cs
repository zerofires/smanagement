﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.dtos
{
    public class NotifyDto
    {
        public NotifyDto()
        {
            cdate = DateTime.Now;
            level = "信息";
        }

        public int id { get; set; }
        public string? username { get; set; }
        public string? name { get; set; }

        public string? title { get; set; }


        [Description("PublishType")]
        public string? type { get; set; }

        [Description("PublishLevel")]
        public string? level { get; set; }

        public string? content { get; set; }

        public DateTime cdate { get; set; }
    }
}
