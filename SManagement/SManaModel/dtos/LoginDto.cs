﻿
using System.ComponentModel;

namespace SManaModel.dtos
{
    public class LoginDto
    {
        [DefaultValue("account")]
        public string type { get; set; }

        [DefaultValue("admin")]
        public string name { get; set; }

        [DefaultValue("123456")]
        public string pwd { get; set; }
    }
}
