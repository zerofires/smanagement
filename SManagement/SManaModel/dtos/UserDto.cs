﻿using System.ComponentModel;

using SManaModel.entities;

namespace SManaModel.dtos
{
    public class UserReqDto : SearchPageDto
    {

        [DefaultValue("student")]
        public string type { get; set; }
    }

    public class UserSelfDto
    {

        [DefaultValue("student")]
        public string type { get; set; }

        public int id { get; set; }
    }

    public class UserDelDto
    {
        public string Id { get; set; } = String.Empty;

        [DefaultValue("student")]
        public string type { get; set; } = "student";

        public IList<int>? getIDs()
        {
            if (string.IsNullOrEmpty(Id))
                return null;

            var list = new List<int>();
            var splits = Id.Split(',');
            foreach (var pair in splits)
            {
                if (int.TryParse(pair, out int id))
                    list.Add(id);
            }
            return list;
        }
    }

    public class UserShowDto
    {
        //[DefaultValue("0")]
        public int id { get; set; } = 0;

        //[DefaultValue("201821184")]
        public string? username { get; set; }

        //[DefaultValue("hello")]
        public string? name { get; set; }

        //[DefaultValue("15445789898")]
        public string? phone { get; set; }
        public string? email { get; set; }
        public string? address { get; set; }
        public string? cls { get; set; }
        public string? politics { get; set; }

        [Description("AvatarUrl")]
        public string? avatar_url { get; set; }
        public DateTime? cdate { get; set; }
    }

    public class UserStudentDto : UserShowDto
    {
        [Description(nameof(StudentEntity.Teacher_Id))]
        public int? tid { get; set; }
        public string? tname { get; set; }

        [Description(nameof(StudentEntity.Teacher2_Id))]
        public int? t2id { get; set; }
        public string? t2name { get; set; }
    }

    public class UserInfoDto : UserShowDto
    {
        [DefaultValue("STUDENT")]
        public string type { get; set; } = "STUDENT";
    }


    public class UserAddDto : UserShowDto
    {
        public UserAddDto()
        {
            cdate = DateTime.Now;
        }

        [DefaultValue("student")]
        public string type { get; set; } = "student";

        [Description("Password")] public string pwd { get; set; } = "123456";
    }

    public class UserUpdateDto : UserShowDto
    {
        public UserUpdateDto()
        {
            cdate = DateTime.Now;
        }

        [DefaultValue("student")]
        public string type { get; set; } = "student";
    }


    public class TeacherDto : UserShowDto
    {
        [DefaultValue("student")]
        public string type { get; set; } = "student";

    }

    public class StudentDto : UserShowDto
    {
        [DefaultValue("student")]
        public string type { get; set; } = "student";

        public string level { get; set; }
    }

    public class AccountDto : UserShowDto
    {
        [DefaultValue("student")]
        public string type { get; set; } = "student";
    }
}
