﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.dtos
{
    public class ActiveDto
    {
        public int id { get; set; }

        [Description("TeacherId")]
        public int tid { get; set; }

        public string? title { get; set; }

        public string? content { get; set; }

        [Description("Description")]
        public string? descr { get; set; }

        [Description("FilePath")]
        public string? file { get; set; }

        public DateTime cdate { get; set; }
    }
}
