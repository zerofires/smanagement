﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.reqs
{
    public class AssignmentReq
    {
        public int StuId { get; set; }
        public int TeaId { get; set; }
        public int Order { get; set; }
    }
}
