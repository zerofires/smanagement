﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.reqs
{
    public class ApproveReq
    {
        //[DefaultValue("student")]
        //public string type { get; set; }

        [Description("ApplyPosition")]
        public string? position { get; set; }

        public string? title { get; set; }

        public string? reason { get; set; }
    }


    public class ApproveUpdateReq : ApproveReq
    {
        public int id { get; set; }
    }
}
