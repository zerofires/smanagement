﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SManaModel.reqs
{
    public class PwdReq
    {
        public string type { get; set; }
        public string pwd { get; set; }
        public string newPwd { get; set; }
    }
}
