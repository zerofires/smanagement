﻿using TestConsole;

var p1 = new RecordPerson
{
    Name = "Tom",
    Age = 12,
};
Console.WriteLine(p1);

var p2 = p1 with { Age = 10 };
Console.WriteLine(p2);
Console.WriteLine($"p1 Equals p2 =:{p1 == p2}");

var p3 = new RecordPerson() { Name = "Tom", Age = 12 };
Console.WriteLine(p3);
Console.WriteLine($"p1 Equals p3 =:{p1 == p3}");
Console.WriteLine($"p2 Equals p3 =:{p2 == p3}");

p2.Age = 12;
Console.WriteLine(p2);
Console.WriteLine($"p1 Equals p3 =:{p1 == p3}");
Console.WriteLine($"p2 Equals p3 =:{p2 == p3}");


RecordPerson2 p4 = new("Tom", 12);
Console.WriteLine(p4);

var vla = EqualityComparer<Type>.Default.GetHashCode(typeof(RecordPerson));
Console.WriteLine(vla);