﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    record RecordPerson
    {
        public string? Name { get; set; }
        public int Age { get; set; }
    }

    record RecordPerson2(string Name, int Age);
}
