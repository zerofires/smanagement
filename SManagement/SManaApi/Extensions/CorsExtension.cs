﻿namespace SManaApi.Extensions
{
    public static class CorsExtension
    {
        public static void AddCorsService(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build());
            });
        }
    }
}
