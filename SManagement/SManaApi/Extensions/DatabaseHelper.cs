﻿using SManaApi.Services;

using System;
using System.Data;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Microsoft.EntityFrameworkCore.Storage;
using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

namespace SManaApi.Extensions
{
    public static class DatabaseHelper
    {
        public static T BeginTransaction<T>(this MyDbContext? dbContext, Func<MyDbContext, IDbContextTransaction, T> func)
        {
            using (var trans = dbContext.Database.BeginTransaction())
            {
                try
                {
                    var data = func.Invoke(dbContext, trans);
                    return data;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    //TODO 日志记录
                    throw;
                }
            }
        }
    }
}
