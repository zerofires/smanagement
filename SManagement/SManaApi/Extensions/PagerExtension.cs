﻿using SManaApi.Services;

using System;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.TagHelpers.Cache;
using Microsoft.EntityFrameworkCore;
using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

namespace SManaApi.Extensions
{
    public static class PagerExtension
    {
        /// <summary>
        /// 分页
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="pageDto"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<TEntity>? Pager<TEntity>(this SearchPageDto pageDto, IQueryable<TEntity>? entities)
        {
            var servicePageComponent = ServicePageComponent<ApproveEntity, DateTime>.Create(
                PageComponent.Create(pageDto),
                entity => entity.CDate,
                entity => true,
                false);

            var outEntities = entities
                ?.Skip(servicePageComponent.PageComponent.SkipSize)
                .Take(servicePageComponent.PageComponent.PageSize);
            return outEntities;
        }

        public static IQueryable<TEntity>? Pager<TEntity, TKey>(this IQueryable<TEntity>? entities, ServicePageComponent<TEntity, TKey> pageComponent) where TEntity : class
        {
            var outEntities = entities
                ?.Skip(pageComponent.PageComponent.SkipSize)
                .Take(pageComponent.PageComponent.PageSize)
                .AsNoTracking();
            return outEntities;
        }
    }
}
