﻿using SManaApi.Services;

using System;
using System.Reflection;

namespace SManaApi.Extensions
{
    public static class ServicesExtension
    {
        public static void AddProjectServices(this IServiceCollection services)
        {

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var item in assemblies)
            {
                if (item.FullName.Contains("SManaApi"))
                {
                    foreach (var mo in item.Modules)
                    {
                        foreach (var t in mo.Assembly.ExportedTypes)
                        {
                            var tn = t.FullName;
                            if (t.IsClass && tn.EndsWith("Service", StringComparison.OrdinalIgnoreCase))
                            {
                                services.AddTransient(Type.GetType(tn));
                            }
                        }
                    }

                    break;
                }
            }


            //services.AddTransient<AccountService>();
            //services.AddTransient<TeacherService>();
            //services.AddTransient<StudentService>();
            //services.AddTransient<FileService>();
            //services.AddTransient<LoginService>();
            //services.AddTransient<NotifyService>();
            //services.AddTransient<ApproveService>();
            //services.AddTransient<ActiveService>();
            //services.AddTransient<NotifyService>();
            //services.AddTransient<JwtService>();
        }
    }
}
