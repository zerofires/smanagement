﻿using SManaApi.Helpers.Enums;

using SManaModel.utils;

namespace SManaApi.Helpers
{
    public static class UserHelper
    {
        public static bool IsInUserType(string type,out UserTypeEnum userType)
        {
            userType = default;
            var enums = Enum.GetNames<UserTypeEnum>();
            foreach (var item in enums)
            {
                if (item.Equals(type, StringComparison.InvariantCultureIgnoreCase))
                {
                    userType = item.ToEnum<UserTypeEnum>();
                    return true;
                }
            }
            return false;
        }

        public static OutObj? SelectTypeAction<Dto, OutObj>(
            this string type,
            Dto dto,
            Func<Dto, OutObj> studentFunc,
            Func<Dto, OutObj> teacherFunc,
            Func<Dto, OutObj> accountFunc)
        {
            var r = type.ToLower() switch
            {
                "student" => studentFunc.Invoke(dto),
                "teacher" => teacherFunc.Invoke(dto),
                "account" => accountFunc.Invoke(dto),
                _ => default
            };
            return r;
        }
    }
}
