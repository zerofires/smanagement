﻿namespace SManaApi.Helpers.Enums
{
    public enum UserTypeEnum
    {
        Student,
        Teacher,
        Account
    }
}
