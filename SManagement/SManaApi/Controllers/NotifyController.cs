﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using SManaApi.Services;

using SManaModel;
using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;
using SManaModel.utils;

using System;
using Microsoft.EntityFrameworkCore;

namespace SManaApi.Controllers
{
    /// <summary>
    /// 通知模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class NotifyController : MasterController<NotifyController>
    {
        private readonly JwtService _jwtService;
        private readonly NotifyService _notifyService;

        public NotifyController(
            ILogger<NotifyController> logger,
            MyDbContext context,
            NotifyService notifyService,
            JwtService jwtService) : base(logger, context)
        {
            _notifyService = notifyService;
            _jwtService = jwtService;
        }

        /// <summary>
        /// 获取通知信息
        /// </summary>
        /// <param name="generalPageDto"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult Get([FromQuery] SearchPageDto? generalPageDto)
        {
            if (generalPageDto is null)
            {
                return ApiResult.FailOfParams();
            }

            var servicePageComponent = ServicePageComponent<NotifyEntity, DateTime>.Create(
                PageComponent.Create(generalPageDto),
                entity => entity.CDate,
                entity => true,
                false);

            //var list = _notifyService.GetNotifiesByPage(servicePageComponent);
            var outList = _notifyService
                .GetEntitiesByPage(servicePageComponent)
                ?.Include(t => t.Account)
                .Select(t => new NotifyDto
                {
                    id = t.Id,
                    username = t.Account.UserName,
                    name = t.Account.Name,
                    content = t.Content,
                    level = t.PublishLevel,
                    cdate = t.CDate,
                    title = t.Title,
                    type = t.PublishType,
                })
                .ToList();
            var count = _notifyService.GetCount();

            return ApiResult.Page(outList!, count);
        }


        /// <summary>
        /// 添加通知
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPost]
        public ApiResult Add([FromBody] NotifyDto dto)
        {
            if (dto is null)
            {
                throw new ArgumentNullException(nameof(dto));
            }

            var entity = dto.ConvertTo<NotifyEntity>();
            var stuId = _jwtService.GetId(HttpContext);
            entity.PublishId = stuId;
            entity.PublishType = _jwtService.GetType(HttpContext);
            var n = _notifyService.Add(entity);
            return ApiResult.Ok(n.ToString());
        }

        /// <summary>
        /// 删除通知
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public ApiResult Delete([FromQuery] string id)
        {
            if (id is null)
            {
                return ApiResult.FailOfParams();
            }

            var n = 0;
            id.Split(',', StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .ForEach(id_str =>
                {
                    if (int.TryParse(id_str, out var i))
                    {
                        n += _notifyService.DeleteEntity(entity => entity.Id == i);
                    }
                });
            return ApiResult.Judge(n);
        }
    }
}
