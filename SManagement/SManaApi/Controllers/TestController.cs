﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.ComponentModel;

namespace SManaApi.Controllers
{
    /// <summary>
    /// 测试接口
    /// </summary>
    [Route("/api/test/[Action]")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public ActionResult hello()
        {
            return Ok("hello");
        }


        [HttpGet]
        public ActionResult error()
        {
            throw new Exception(DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "测试自定义异常中间件是否生效!");
        }


        [HttpGet]
        public ActionResult dict_test([FromQuery] IDictionary<string, string> data)
        {

            return Ok(data);
        }

        public class P1
        {
            [DefaultValue("hello")]
            public string name { get; set; }

            [FromQuery]
            public IDictionary<string, string>? dict_two { get; set; }

            public Dict dict { get; set; }

        }

        public class Dict
        {
            [FromQuery]
            public IDictionary<string, string>? dict { get; set; }
        }

        [HttpGet]
        public ActionResult dict_test2([FromQuery] P1 data)
        {

            return Ok(data);
        }
    }
}
