﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using SManaModel;
using SManaModel.entities;

namespace SManaApi.Controllers
{
    /// <summary>
    /// 角色模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Obsolete("未实际使用")]
    public class RoleController : MasterController<RoleController>
    {
        public RoleController(ILogger<RoleController> logger, MyDbContext context) : base(logger,context)
        {
        }

        [HttpGet]
        public ApiResult apiResult()
        {
            return ApiResult.Ok();
        }
    }
}
