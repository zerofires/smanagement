﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using SManaModel.entities;

namespace SManaApi.Controllers
{
    /// <summary>
    /// 报道模块
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Obsolete("未实际使用")]
    public class ReportController : MasterController<ReportController>
    {
        public ReportController(ILogger<ReportController> logger, MyDbContext context) : base(logger,context)
        {
        }
    }
}
