﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using SManaApi.Services;

using SManaModel;
using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;
using SManaModel.reqs;
using SManaModel.utils;

namespace SManaApi.Controllers
{
    /// <summary>
    /// 进程模块
    /// </summary>
    public class ProcessController : MasterController<ProcessController>
    {
        protected readonly ProcessService _processService;
        protected readonly JwtService _jwtService;

        public ProcessController(ILogger<ProcessController> logger, MyDbContext dbContext, ProcessService processService, JwtService jwtService) : base(logger, dbContext)
        {
            _processService = processService;
            _jwtService = jwtService;
        }

        /// <summary>
        /// 获取发展进程
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetList()
        {
            var stuId = _jwtService.GetId(HttpContext);
            if (stuId > 0)
            {
                var entities = _processService.GetEntities();
                var processEntities = await entities?.Where(t => t.StudentId == stuId).ToListAsync();
                return ApiResult.Ok(data: processEntities);
            }
            return ApiResult.Fail("没有任何数据");
        }


    }
}
