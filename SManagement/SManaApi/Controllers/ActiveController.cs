﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using SManaApi.Services;

using SManaModel;
using SManaModel.dtos;
using SManaModel.entities;

using System;

namespace SManaApi.Controllers
{
    // 思想报告模块
    //[Route("api/[controller]")]
    //[ApiController]
    //[Obsolete("未实际使用")]
    //public class ActiveController : MasterController<ActiveController>
    //{
    //    private readonly ActiveService _activeService;

    //    public ActiveController(ILogger<ActiveController> logger, MyDbContext context, ActiveService activeService) : base(logger, context)
    //    {
    //        _activeService = activeService;
    //    }

    //    [HttpGet]
    //    public ApiResult Get([FromQuery] SearchPageDto generalPageDto)
    //    {
    //        var r = general_get<SearchPageDto, ActiveEntity, DateTime, ActiveDto>(generalPageDto, _activeService, entity => entity.CDate);
    //        return r;
    //    }


    //    [HttpPost]
    //    public ApiResult Add([FromBody] NotifyDto dto)
    //    {
    //        var r = general_add(dto, _activeService);
    //        return r;
    //    }


    //    [HttpDelete]
    //    public ApiResult Delete([FromQuery] string id)
    //    {
    //        if (id is null)
    //        {
    //            return ApiResult.FailOfParams();
    //        }

    //        var n = 0;
    //        id.Split(',', StringSplitOptions.RemoveEmptyEntries)
    //            .ToList()
    //            .ForEach(id_str =>
    //            {
    //                if (int.TryParse(id_str, out var i))
    //                {
    //                    n += _activeService.DeleteEntity(entity => entity.Id == i);
    //                }
    //            });
    //        return ApiResult.Judge(n);
    //    }

    //    [HttpPut]
    //    public ApiResult Update([FromBody] ActiveDto dto)
    //    {
    //        var r = general_update(dto, _activeService);
    //        return r;
    //    }
    //}
}
