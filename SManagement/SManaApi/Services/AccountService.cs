﻿using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

using static System.String;

namespace SManaApi.Services
{
    public class AccountService : UserService<AccountEntity>
    {
        public AccountService(MyDbContext dbContext) : base(dbContext)
        {
        }

        public override int? GetCount()
        {
            return _dbContext?.Account?.Count();
        }

        public override IList<AccountEntity>? GetUsers(int pageNum, string? condition = null)
        {
            var count = GetCount();
            var conditions = PageComponent.GetCondition(condition);
            if (!count.HasValue)
            {
                return null;
            }
            var page = PageComponent.Create(pageNum, count.Value);
            var list = _dbContext?.Account?.OrderByDescending(x => x.CDate).Skip(page.SkipSize).Take(page.PageSize).ToList();
            list?.ForEach(x => x.Password = Empty);
            return list;
        }

        public override IQueryable<AccountEntity>? GetUsersByPage(UserReqDto userGetDto)
        {
            if (userGetDto is null)
            {
                throw new ArgumentNullException(nameof(userGetDto));
            }

            var servicePageComponent = ServicePageComponent<AccountEntity, DateTime>.Create(
                                PageComponent.Create(userGetDto),
                                entity => entity.CDate,
                                null,
                                false);
            var list = base.GetEntitiesByPage<DateTime>(servicePageComponent);
            if (list == null)
                throw new ArgumentNullException(string.Format(format: @"list is null where from {0}.GetEntitiesByPage<TEntity>()",
                    nameof(AccountService)));

            var searchConditions = userGetDto.Conditions;
            if (searchConditions == null)
            {
                return list;
            }
            else
            {
                foreach (var condition in searchConditions)
                {
                    var key = condition.Key;
                    var value = condition.Value;
                    if (IsNullOrEmpty(value))
                        continue;
                    list = key switch
                    {
                        "address" => list.Where(entity => entity.Address.Contains(value)),
                        "phone" => list.Where(entity => entity.Phone.Contains(value)),
                        "name" => list.Where(entity => entity.UserName.Contains(value) || entity.Name.Contains(value)),
                        _ => list
                    };
                }
                return list;
            }
        }
    }
}
