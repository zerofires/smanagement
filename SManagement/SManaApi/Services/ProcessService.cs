﻿using System.Diagnostics;

using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

namespace SManaApi.Services
{
    public class ProcessService : BaseService<ProcessEntity>
    {
        public ProcessService(MyDbContext dbContext) : base(dbContext)
        {
        }
    }
}
