﻿using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

namespace SManaApi.Services
{
    public class TeacherService : UserService<TeacherEntity>
    {
        public TeacherService(MyDbContext dbContext) : base(dbContext)
        {
        }

        public override int? GetCount()
        {
            return _dbContext?.Teacher?.Count();
        }

        public override IList<TeacherEntity>? GetUsers(int pageNum, string? condition = null)
        {
            var count = GetCount();
            var conditions = PageComponent.GetCondition(condition);
            if (!count.HasValue)
            {
                return null;
            }
            var page = PageComponent.Create(pageNum, count.Value);
            var list = _dbContext?.Teacher?.OrderByDescending(x => x.CDate).Skip(page.SkipSize).Take(page.PageSize).ToList();
            list?.ForEach(x => x.Password = String.Empty);
            return list;
        }

        public override IQueryable<TeacherEntity>? GetUsersByPage(UserReqDto userGetDto)
        {
            if (userGetDto is null)
            {
                throw new ArgumentNullException(nameof(userGetDto));
            }

            var servicePageComponent = ServicePageComponent<TeacherEntity, DateTime>.Create(
                                PageComponent.Create(userGetDto),
                                entity => entity.CDate,
                                null,
                                false);

            var list = base.GetEntitiesByPage<DateTime>(servicePageComponent);
            if (list == null)
                throw new ArgumentNullException(string.Format("list is null where from {}.GetEntitiesByPage<TEntity>()", nameof(TeacherService)));

            var searchConditions = userGetDto.Conditions;
            if (searchConditions == null)
            {
                return list;
            }
            else
            {
                foreach (var condition in searchConditions)
                {
                    var key = condition.Key;
                    var value = condition.Value;
                    if (string.IsNullOrEmpty(value))
                        continue;
                    list = key switch
                    {
                        "address" => list.Where(entity => entity.Address.Contains(value)),
                        "email" => list.Where(entity => entity.Email.Contains(value)),
                        "phone" => list.Where(entity => entity.Phone.Contains(value)),
                        "name" => list.Where(entity => entity.UserName.Contains(value) || entity.Name.Contains(value)),
                        _ => list
                    };
                }
                return list;
            }
        }
    }
}
