﻿using SManaModel.entities;

using System;

namespace SManaApi.Services
{
    public class LoginService
    {
        protected readonly MyDbContext _dbContext;

        public LoginService(MyDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public object? Login(string type, string name, string pwd)
        {
            object person = type switch
            {
                "student" => _dbContext.Student!.Where(x => x.UserName == name && x.Password == pwd)?.FirstOrDefault(),
                "teacher" => _dbContext.Teacher!.Where(x => x.UserName == name && x.Password == pwd)?.FirstOrDefault(),
                "account" => _dbContext.Account!.Where(x => x.UserName == name && x.Password == pwd)?.FirstOrDefault(),
                _ => null
            };
            return person;
        }
    }
}
