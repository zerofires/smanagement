﻿using SManaModel.entities;

using System;

namespace SManaApi.Services
{
    public class FileService : BaseService<FileEntity>
    {
        public FileService(MyDbContext dbContext) : base(dbContext)
        {
        }

        public int Add(FileEntity entity)
        {
            _dbContext.File.Add(entity);
            var n = _dbContext.SaveChanges();
            return n;
        }

        public int AddRange(IList<FileEntity> entitys)
        {
            _dbContext.File.AddRange(entitys);
            var n = _dbContext.SaveChanges();
            return n;
        }

        public int Delete(int id)
        {
            var entity = _dbContext?.File?.Where(t => t.Id == id).FirstOrDefault();
            if (entity == null)
                return 0;

            entity.Is_Delete = 1;
            _dbContext.Update(entity);
            var n = _dbContext.SaveChanges();
            return n;
        }
    }
}
