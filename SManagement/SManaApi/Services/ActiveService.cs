﻿using SManaModel.entities;

namespace SManaApi.Services
{
    public class ActiveService : BaseService<ActiveEntity>
    {
        public ActiveService(MyDbContext dbContext) : base(dbContext)
        {
        }
    }
}
