﻿using SManaModel.dtos;
using SManaModel.entities;

namespace SManaApi.Services
{
    public abstract class UserService<Entity> : BaseService<Entity> where Entity : class, new()
    {
        protected UserService(MyDbContext dbContext) : base(dbContext)
        {
        }

        public abstract IList<Entity>? GetUsers(int pageNum, string? condition = null);
        public abstract IQueryable<Entity>? GetUsersByPage(UserReqDto userGetDto);
        public abstract int? GetCount();
    }
}
