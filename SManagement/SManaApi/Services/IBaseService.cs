﻿using SManaModel.models;

using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace SManaApi.Services
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        int GetCount(Expression<Func<TEntity, bool>>? whereExpression=default);

        IQueryable<TEntity>? GetEntitiesByPage<TKey>(ServicePageComponent<TEntity, TKey> servicePageComponent);

        IQueryable<TEntity>? GetEntity(Expression<Func<TEntity, bool>> whereExpression);

        int DeleteEntity(Expression<Func<TEntity, bool>>? whereExpression, DbContext? dbContext = default);

        int AddEntity(TEntity? entity, DbContext? dbContext = default);

        int UpdateEntity(TEntity? entity, DbContext? dbContext = default);
    }
}
