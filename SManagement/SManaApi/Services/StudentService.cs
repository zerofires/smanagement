﻿using Microsoft.EntityFrameworkCore;

using SManaModel.dtos;
using SManaModel.entities;
using SManaModel.models;

namespace SManaApi.Services
{
    public class StudentService : UserService<StudentEntity>
    {
        public StudentService(MyDbContext dbContext) : base(dbContext)
        {
        }

        public override int? GetCount()
        {
            return _dbContext?.Student?.Count();
        }

        public override List<StudentEntity>? GetUsers(int pageNum, string? condition = null)
        {
            var conditions = PageComponent.GetCondition(condition);
            var count = GetCount();
            if (!count.HasValue)
            {
                return null;
            }

            var page = PageComponent.Create(pageNum, count.Value);
            var list = _dbContext?.Student?.OrderByDescending(x => x.CDate).Skip(page.SkipSize).Take(page.PageSize).ToList();
            list?.ForEach(x => x.Password = String.Empty);
            return list;
        }

        public override IQueryable<StudentEntity>? GetUsersByPage(UserReqDto userGetDto)
        {
            if (userGetDto is null)
            {
                throw new ArgumentNullException(nameof(userGetDto));
            }

            // 页面服务组件
            var servicePageComponent = ServicePageComponent<StudentEntity, DateTime>.Create(
                                PageComponent.Create(userGetDto),
                                entity => entity.CDate,
                                null,
                                false);
            var list = base.GetEntitiesByPage<DateTime>(servicePageComponent);
            if (list == null)
                throw new ArgumentNullException(string.Format("list is null where from {}.GetEntitiesByPage<TEntity>()", nameof(StudentService)));

            var searchConditions = userGetDto.Conditions;
            if (searchConditions == null || searchConditions.Count < 1)
            {
                return list;
            }
            else
            {
                // 过滤条件
                foreach (var condition in searchConditions)
                {
                    var key = condition.Key;
                    var value = condition.Value;
                    if (string.IsNullOrEmpty(value))
                        continue;
                    list = key switch
                    {
                        "address" => list.Where(entity => entity.Address.Contains(value)),
                        "email" => list.Where(entity => entity.Email.Contains(value)),
                        "phone" => list.Where(entity => entity.Phone.Contains(value)),
                        "cls" => list.Where(entity => entity.Cls.Contains(value)),
                        "politics" => list.Where(entity => entity.Politics.Contains(value)),
                        "name" => list.Where(entity => entity.UserName.Contains(value) || entity.Name.Contains(value)),
                        _ => list
                    };
                }
                return list;
            }
        }

        /// <summary>
        /// 分配培养人
        /// </summary>
        /// <param name="stuId">学生id</param>
        /// <param name="teaId">老师id</param>
        /// <param name="order">培养人序号</param>
        /// <returns></returns>
        public int AssignmentTeacher(int stuId, int teaId, int order)
        {
            var studentEntity = _dbContext.Student!.FirstOrDefault(t => t.Id == stuId);
            if (studentEntity == null)
                return 0;

            if (order == 1)
            {
                studentEntity.Teacher_Id = teaId;
            }
            else if (order == 2)
            {
                studentEntity.Teacher2_Id = teaId;
            }

            _dbContext.Student.Update(studentEntity);
            var n = _dbContext.SaveChanges();
            return n;
        }
    }
}
